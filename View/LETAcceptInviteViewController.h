//
//  LETAcceptInviteViewController.h
//  Lets
//
//  Created by Surik Sarkisyan on 29/03/2016.
//  Copyright 2016 Lets. All rights reserved.
//

#import "LETBaseViewController.h"

#import "LETAcceptInviteViewInput.h"
#import "SARRoundedImage.h"

@class LETButton;

@protocol LETAcceptInviteViewOutput;

@interface LETAcceptInviteViewController : LETBaseViewController <LETAcceptInviteViewInput>

@property (nonatomic, weak) IBOutlet LETButton *inviteButton;
@property (nonatomic, weak) IBOutlet UILabel *messageLabel;
@property (nonatomic, weak) IBOutlet SARRoundedImage *userAvatarImageView;
@property (weak, nonatomic) IBOutlet UIImageView *eventPosterImageView;
@property (weak, nonatomic) IBOutlet UILabel *eventNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *eventDetailLabel;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *inviteButtonHeightConstraint;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicatorView;

@property (nonatomic, strong) id<LETAcceptInviteViewOutput> output;

@end