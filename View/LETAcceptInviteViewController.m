//
//  LETAcceptInviteViewController.m
//  Lets
//
//  Created by Surik Sarkisyan on 29/03/2016.
//  Copyright 2016 Lets. All rights reserved.
//

#import "LETAcceptInviteViewController.h"

#import "LETAcceptInviteViewOutput.h"

#import "LETButton.h"
#import "LETMatchUser.h"
#import "LETImage.h"
#import "LETEvent.h"
#import "LETFilm.h"

#import <AFNetworking/UIImageView+AFNetworking.h>
#import "NSDate+LETFormat.h"
#import "LETAnimationDelaysConstants.h"

@implementation LETAcceptInviteViewController

#pragma mark - Методы жизненного цикла
- (void)viewDidLoad 
{
	[super viewDidLoad];

	[self.output didTriggerViewReadyEvent];
}

- (void)viewWillAppear:(BOOL)animated
{    
    [super viewWillAppear:animated];

    [self.output didTriggerViewWillAppearEvent];
}

#pragma mark - LETAcceptInviteViewInput
- (void)hideNavigationBar
{
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)setInviteButtonTitle:(NSString *)title
{
    [self.inviteButton setTitle:title forState:UIControlStateNormal];
}

- (void)setMessageLabelText:(NSString *)title
{
    self.messageLabel.text = title;
}

- (void)showIviteDetailsWithUser:(LETMatchUser *)user
{
#warning PLACEHOLDER!!!
    NSURLRequest *request = [NSURLRequest requestWithURL:user.mainPhoto.url];
    [self.userAvatarImageView setImageWithURLRequest:request placeholderImage:nil success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull image) {
        self.userAvatarImageView.image = image;
        self.activityIndicatorView.hidden = NO;
        [self.activityIndicatorView startAnimating];
    } failure:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, NSError * _Nonnull error) {
        self.activityIndicatorView.hidden = YES;
        [self.activityIndicatorView stopAnimating];
    }];
    [self.eventPosterImageView setImageWithURL:[NSURL URLWithString:user.event.film.previewImageURL] placeholderImage:nil];
    self.eventNameLabel.text = user.event.film.name;
    NSString *dateString = [user.event.date dateStringForFilm];
    NSString *timeString = [user.event.date dateStringWithFormat:@"HH:mm"];
    NSString *eventDetail = [NSString stringWithFormat:@"%@, %@, %@", dateString, user.event.placeName, timeString];
    self.eventDetailLabel.text = eventDetail;
}

- (void)hideAcceptInviteButton
{
    self.inviteButtonHeightConstraint.constant = 0.0;
    
    [UIView animateWithDuration:kLETAnimationDefaultDelay animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
    	self.inviteButton.hidden = YES;
    }];
}

#pragma mark - Actions
- (IBAction)inviteButtonPressed:(id)sender
{
    [self.output inviteButtonPressed];
}

- (IBAction)writeMessageButtonPressed:(id)sender
{
    [self.output writeMessageButtonPressed];
}

- (IBAction)closeButtonPressed:(id)sender
{
    [self.output closeButtonPressed];
}

@end