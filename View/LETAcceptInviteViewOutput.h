//
//  LETAcceptInviteViewOutput.h
//  Lets
//
//  Created by Surik Sarkisyan on 29/03/2016.
//  Copyright 2016 Lets. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol LETAcceptInviteViewOutput <NSObject>

- (void)didTriggerViewReadyEvent;
- (void)didTriggerViewWillAppearEvent;
- (void)writeMessageButtonPressed;
- (void)inviteButtonPressed;
- (void)closeButtonPressed;

@end