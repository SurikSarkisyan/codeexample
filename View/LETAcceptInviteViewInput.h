//
//  LETAcceptInviteViewInput.h
//  Lets
//
//  Created by Surik Sarkisyan on 29/03/2016.
//  Copyright 2016 Lets. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "LETBaseViewInput.h"

@class LETMatchUser;

@protocol LETAcceptInviteViewInput <LETBaseViewInput>

- (void)hideNavigationBar;
- (void)setInviteButtonTitle:(NSString *)title;
- (void)setMessageLabelText:(NSString *)title;
- (void)showIviteDetailsWithUser:(LETMatchUser *)user;
- (void)hideAcceptInviteButton;

@end