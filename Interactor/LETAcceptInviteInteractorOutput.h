//
//  LETAcceptInviteInteractorOutput.h
//  Lets
//
//  Created by Surik Sarkisyan on 29/03/2016.
//  Copyright 2016 Lets. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol LETAcceptInviteInteractorOutput <NSObject>

- (void)inviteMessageConfigured:(NSString *)message;
- (void)acceptInviteWasFinishedWithSuccess:(NSString *)successMessage;
- (void)acceptInviteWasFailedWithError:(NSError *)error;

@end