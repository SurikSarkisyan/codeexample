//
//  LETAcceptInviteInteractorInput.h
//  Lets
//
//  Created by Surik Sarkisyan on 29/03/2016.
//  Copyright 2016 Lets. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "LETUserStatusDefines.h"
#import "LETUserGenderType.h"

@class LETMatchUser;
@class LETPairUser;

@protocol LETAcceptInviteInteractorInput <NSObject>

- (void)configureIncomingInviteMessageForGender:(LETGenderType)genderType userName:(NSString *)userName;
- (void)configureOutgoingInviteMessageForGender:(LETGenderType)genderType userName:(NSString *)userName;
- (void)acceptInviteForUserWithID:(NSNumber *)userID;
- (LETPairUser *)convertMatchUserToPair:(LETMatchUser *)user;
- (void)markAsReadedUserWithID:(NSNumber *)userID userStatus:(LETUserStatus)status;

@end