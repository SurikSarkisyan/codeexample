//
//  LETAcceptInviteInteractor.m
//  Lets
//
//  Created by Surik Sarkisyan on 29/03/2016.
//  Copyright 2016 Lets. All rights reserved.
//

#import "LETAcceptInviteInteractor.h"

#import "LETAcceptInviteInteractorOutput.h"
#import "NSString+Gender.h"
#import "LETServiceAssembly.h"
#import "LETCommunicationServiceProtocol.h"

static NSString *const kLETIncomingInvite = @"%@ %@\nвас в кино!";
static NSString *const kLETOutgoingInvite = @"%@ %@\nваше приглашение!";

@implementation LETAcceptInviteInteractor

#pragma mark - LETAcceptInviteInteractorInput
- (void)configureIncomingInviteMessageForGender:(LETGenderType)genderType userName:(NSString *)userName
{
    NSString *inviteString = [NSString let_incomingInviteForGender:genderType];
    [self.output inviteMessageConfigured:[NSString stringWithFormat:kLETIncomingInvite, userName, inviteString]];
}

- (void)configureOutgoingInviteMessageForGender:(LETGenderType)genderType userName:(NSString *)userName
{
    NSString *inviteString = [NSString let_outgoingInviteForGender:genderType];
    [self.output inviteMessageConfigured:[NSString stringWithFormat:kLETOutgoingInvite, userName, inviteString]];
}

- (void)acceptInviteForUserWithID:(NSNumber *)userID
{
    [self.communicationService acceptInviteForUserWithID:userID completion:^(NSString *successMessage, NSError *error) {
        if ( error )
        {
            [self.output acceptInviteWasFailedWithError:error];
            return;
        }
        
        [self.output acceptInviteWasFinishedWithSuccess:successMessage];
    }];
}

- (LETPairUser *)convertMatchUserToPair:(LETMatchUser *)user
{
    return [self.communicationService convertMatchUserToPair:user];
}

- (void)markAsReadedUserWithID:(NSNumber *)userID userStatus:(LETUserStatus)status
{
    [self.communicationService markAsReadedUserWithID:userID userStatus:status];
}

@end