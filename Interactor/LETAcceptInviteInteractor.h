//
//  LETAcceptInviteInteractor.h
//  Lets
//
//  Created by Surik Sarkisyan on 29/03/2016.
//  Copyright 2016 Lets. All rights reserved.
//

#import "LETAcceptInviteInteractorInput.h"

@protocol LETAcceptInviteInteractorOutput;
@protocol LETCommunicationServiceProtocol;

@interface LETAcceptInviteInteractor : NSObject <LETAcceptInviteInteractorInput>

@property (nonatomic, weak) id<LETAcceptInviteInteractorOutput> output;
@property (nonatomic, strong) id<LETCommunicationServiceProtocol> communicationService;

@end