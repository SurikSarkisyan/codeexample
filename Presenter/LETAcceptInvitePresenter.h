//
//  LETAcceptInvitePresenter.h
//  Lets
//
//  Created by Surik Sarkisyan on 29/03/2016.
//  Copyright 2016 Lets. All rights reserved.
//

#import "LETAcceptInviteViewOutput.h"
#import "LETAcceptInviteInteractorOutput.h"
#import "LETAcceptInviteModuleInput.h"

@protocol LETAcceptInviteViewInput;
@protocol LETAcceptInviteInteractorInput;
@protocol LETAcceptInviteRouterInput;

@interface LETAcceptInvitePresenter : NSObject <LETAcceptInviteViewOutput, LETAcceptInviteInteractorOutput, LETAcceptInviteModuleInput>

@property (nonatomic, weak) id<LETAcceptInviteViewInput> view;
@property (nonatomic, strong) id<LETAcceptInviteInteractorInput> interactor;
@property (nonatomic, strong) id<LETAcceptInviteRouterInput> router;

@end