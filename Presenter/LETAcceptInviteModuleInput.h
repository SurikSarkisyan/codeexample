//
//  LETAcceptInviteModuleInput.h
//  Lets
//
//  Created by Surik Sarkisyan on 30.03.16.
//  Copyright © 2016 Lets. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ViperMcFlurry/IntermoduleDataTransfer.h>

@class LETMatchUser;

typedef NS_ENUM(NSUInteger, LETInviteType)
{
    LETIncomingInviteType = 0,
    LETOutgoingInviteType = 1,
    LETIncomingAcceptedInvite = 2
};

@protocol LETAcceptInviteModuleInput <RamblerViperModuleInput>

- (void)showInviteFromUser:(LETMatchUser *)user;
- (void)showAcceptedInviteFromUser:(LETMatchUser *)user;
- (void)showIncomingAcceptedInviteFromUser:(LETMatchUser *)user;

@end
