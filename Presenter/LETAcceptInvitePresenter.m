//
//  LETAcceptInvitePresenter.m
//  Lets
//
//  Created by Surik Sarkisyan on 29/03/2016.
//  Copyright 2016 Lets. All rights reserved.
//

#import "LETAcceptInvitePresenter.h"

#import "LETAcceptInviteViewInput.h"
#import "LETAcceptInviteInteractorInput.h"
#import "LETAcceptInviteRouterInput.h"
#import "LETMatchUser.h"

static NSString *const kLETAcceptInvite = @"Принять приглашение";
static NSString *const kLETBuyTickets = @"Купить билеты";
static NSString *const kLETYouAcceptInvite = @"Вы приняли приглашение";

@interface LETAcceptInvitePresenter ()

@property (nonatomic, strong) LETMatchUser *user;
@property (nonatomic, assign) LETInviteType inviteType;

@end

@implementation LETAcceptInvitePresenter

#pragma mark - LETAcceptInviteModuleInput
- (void)showInviteFromUser:(LETMatchUser *)user
{
    self.inviteType = LETIncomingInviteType;
    self.user = user;
}

- (void)showAcceptedInviteFromUser:(LETMatchUser *)user
{
    self.inviteType = LETOutgoingInviteType;
    self.user = user;
}

- (void)showIncomingAcceptedInviteFromUser:(LETMatchUser *)user
{
    self.inviteType = LETIncomingAcceptedInvite;
    self.user = user;
}

#pragma mark - LETAcceptInviteViewOutput
- (void)didTriggerViewReadyEvent 
{
    if ( self.user.isNew )
    {
        [self.interactor markAsReadedUserWithID:self.user.uid userStatus:self.user.userStatus];
    }
    
    if ( self.inviteType == LETIncomingInviteType )
    {
        [self.view setInviteButtonTitle:kLETAcceptInvite];
        [self.interactor configureIncomingInviteMessageForGender:self.user.genderType userName:self.user.name];
    }
    else if ( self.inviteType == LETOutgoingInviteType )
    {
        [self.view setInviteButtonTitle:kLETBuyTickets];
        [self.interactor configureOutgoingInviteMessageForGender:self.user.genderType userName:self.user.name];
    }
    else if ( self.inviteType == LETIncomingAcceptedInvite )
    {
        [self.view hideAcceptInviteButton];
        [self.view setMessageLabelText:kLETYouAcceptInvite];
    }
    
    [self.view showIviteDetailsWithUser:self.user];
}

- (void)didTriggerViewWillAppearEvent
{
    [self.view hideNavigationBar];
}

- (void)inviteButtonPressed
{
    if ( self.inviteType == LETIncomingInviteType )
    {
        [self.view showHUD];
        [self.interactor acceptInviteForUserWithID:self.user.uid];
    }
    else if ( self.inviteType == LETOutgoingInviteType )
    {
        [self.router openURLForBuyTickets];
    }
}

- (void)writeMessageButtonPressed
{
    LETPairUser *pairUser = [self.interactor convertMatchUserToPair:self.user];
    [self.router showChatModuleWithUser:pairUser];
}

- (void)closeButtonPressed
{
    [self.router closeModule];
}

#pragma mark - LETAcceptInviteInteractorOutput
- (void)acceptInviteWasFinishedWithSuccess:(NSString *)successMessage
{
    [self.view hideHUD];
    [self.view hideAcceptInviteButton];
    [self.view setMessageLabelText:kLETYouAcceptInvite];
    [self.view showSuccessMessageInAlertView:successMessage];
}

- (void)acceptInviteWasFailedWithError:(NSError *)error
{
    [self.view hideHUD];
    [self.view showErrorMessageInAlertView:error.localizedDescription];
}

- (void)inviteMessageConfigured:(NSString *)message
{
    [self.view setMessageLabelText:message];
}

@end