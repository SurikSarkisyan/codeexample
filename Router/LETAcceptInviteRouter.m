//
//  LETAcceptInviteRouter.m
//  Lets
//
//  Created by Surik Sarkisyan on 29/03/2016.
//  Copyright 2016 Lets. All rights reserved.
//

#import "LETAcceptInviteRouter.h"

#import <ViperMcFlurry/ViperMcFlurry.h>

#import "LETStoryboardConstants.h"
#import "LETChatModuleInput.h"

static NSString *const kLETByCardURLScheme = @"bycard://";
static NSString *const kLETByCardSiteURL = @"http://bycard.by";

@implementation LETAcceptInviteRouter

#pragma mark - LETAcceptInviteRouterInput
- (void)closeModule
{
    [self.transitionHandler closeCurrentModule:YES];
}

- (void)showChatModuleWithUser:(LETPairUser *)user
{
    [[self.transitionHandler openModuleUsingSegue:kLETFromAcceptInviteToChatSegue] thenChainUsingBlock:^id<RamblerViperModuleOutput>(id<LETChatModuleInput> moduleInput) {
        [moduleInput showChatWithUser:user];
        
        return nil;
    }];
}

- (void)openURLForBuyTickets
{
    NSURL *bycardURLScheme = [NSURL URLWithString:kLETByCardURLScheme];
    
    if ( [[UIApplication sharedApplication] canOpenURL:bycardURLScheme] )
    {
        [[UIApplication sharedApplication] openURL:bycardURLScheme];
    }
    else
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:kLETByCardSiteURL]];
    }
}

@end
