//
//  LETAcceptInviteRouter.h
//  Lets
//
//  Created by Surik Sarkisyan on 29/03/2016.
//  Copyright 2016 Lets. All rights reserved.
//

#import "LETAcceptInviteRouterInput.h"

@protocol RamblerViperModuleTransitionHandlerProtocol;

@interface LETAcceptInviteRouter : NSObject <LETAcceptInviteRouterInput>

@property (nonatomic, weak) id<RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end