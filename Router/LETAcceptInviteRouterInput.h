//
//  LETAcceptInviteRouterInput.h
//  Lets
//
//  Created by Surik Sarkisyan on 29/03/2016.
//  Copyright 2016 Lets. All rights reserved.
//

#import <Foundation/Foundation.h>

@class LETPairUser;

@protocol LETAcceptInviteRouterInput <NSObject>

- (void)closeModule;
- (void)showChatModuleWithUser:(LETPairUser *)user;
- (void)openURLForBuyTickets;

@end