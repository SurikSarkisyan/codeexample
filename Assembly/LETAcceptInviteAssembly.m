//
//  LETAcceptInviteAssembly.m
//  Lets
//
//  Created by Surik Sarkisyan on 29/03/2016.
//  Copyright 2016 Lets. All rights reserved.
//

#import "LETAcceptInviteAssembly.h"

#import "LETAcceptInviteViewController.h"
#import "LETAcceptInviteInteractor.h"
#import "LETAcceptInvitePresenter.h"
#import "LETAcceptInviteRouter.h"

#import "LETServiceAssembly.h"

@implementation LETAcceptInviteAssembly

- (LETAcceptInviteViewController *)viewAcceptInviteModule 
{
    return [TyphoonDefinition withClass:[LETAcceptInviteViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterAcceptInviteModule]];
                          }];
}

- (LETAcceptInviteInteractor *)interactorAcceptInviteModule 
{
    return [TyphoonDefinition withClass:[LETAcceptInviteInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterAcceptInviteModule]];
                              [definition injectProperty:@selector(communicationService)
                                                    with:[self.serviceAssembly communicationService]];
                          }];
}

- (LETAcceptInvitePresenter *)presenterAcceptInviteModule 
{
    return [TyphoonDefinition withClass:[LETAcceptInvitePresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewAcceptInviteModule]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorAcceptInviteModule]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerAcceptInviteModule]];
                          }];
}

- (LETAcceptInviteRouter *)routerAcceptInviteModule 
{
    return [TyphoonDefinition withClass:[LETAcceptInviteRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewAcceptInviteModule]];
                          }];
}

@end