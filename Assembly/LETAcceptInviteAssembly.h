//
//  LETAcceptInviteAssembly.h
//  Lets
//
//  Created by Surik Sarkisyan on 29/03/2016.
//  Copyright 2016 Lets. All rights reserved.
//

#import <Typhoon/Typhoon.h>

#import <RamblerTyphoonUtils/AssemblyCollector.h>

@class LETServiceAssembly;

@interface LETAcceptInviteAssembly : TyphoonAssembly <RamblerInitialAssembly>

@property (nonatomic, readonly, strong) LETServiceAssembly *serviceAssembly;

@end